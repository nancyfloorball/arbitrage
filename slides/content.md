## Petit guide d'arbitrage à destination des joueurs de Floorball

![Logo Loups Lorrains](images/loupslorrains.png) <!-- .element: class="plain" -->

Septembre 2018 <!-- .element: class="small" -->

[To switch to English, click <a href="#/1">here</a>]



## Small guide to floorball rules for players

![Logo Loups Lorrains](images/loupslorrains.png) <!-- .element: class="plain" -->

September 2018 <!-- .element: style="font_size:small" -->

[Pour aller à la version française, cliquer <a href="#/0">ici</a>]



## &Agrave; propos de ce document <span class="menu-title" style="display: none">&Agrave; propos de ce document</span>

**Objectifs**<!-- .element: style="float:left;color:orange;" -->

- sensibiliser les joueurs à l'importance de l'arbitrage
- rappeler les règles <br/><br/>

**Utilisation**<!-- .element: style="float:left;color:orange;" -->

- presser la touche page haut (&#x21de;) / page bas (&#x21df;) pour basculer vers français/anglais
- presser la flèche gauche/droite pour reculer/avancer dans la présentation (sans changer de langue)



## About this document

**Goals**<!-- .element: style="float:left;color:orange;" -->
<br>
- get players to know that referees matter
- recall floorball rules

**Usage**<!-- .element: style="float:left;color:orange;" -->

- press **page up (&#x21de;) / page down (&#x21df;)** key to switch to French/English
- press left/right arrow to move backward/forward (without changing language)



## Rôle de l'arbitre <span class="menu-title" style="display: none">Rôle de l'arbitre</span>

Faire respecter les règles <br>(garantir l'équité du match)<br><br>Préserver l'intégrité physique des joueurs (protéger)<br><br>Permettre aux joueurs de se concentrer sur le jeu <br>(augmenter le niveau de jeu) <!-- .element: style="float: right; width: 70%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Referee's role

Get players to play by the rules (match equity)<br><br>Preserve players' health (protect)<br><br>Allow players to focus on their game (improve match quality) <!-- .element: style="float: right; width: 60%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Caractéristiques de l'arbitre <span class="menu-title" style="display: none">Caractéristiques de l'arbitre</span>

Est un être humain (peut commettre des erreurs, mérite le respect)<br><br>Participe au bon déroulement du match <br><br>A toujours raison <br>(on joue au coup de sifflet) <!-- .element: style="float: right; width: 80%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Referee's features

Is a human being <br>(may be wrong, deserves respect)<br><br>Is part of the game<br><br>Is always right <br>("listen to the whistle!") <!-- .element: style="float: right; width: 70%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Qualités de l'arbitre <span class="menu-title" style="display: none">Qualités de l'arbitre</span>

Est présent et ferme<br>(communique avec les joueurs.euses, <br>ne siffle QUE ce qu'il/elle voit!) <br><br>A une bonne lecture du jeu <br>(sait se positionner, analyse le comportement des joueurs.euses) <!-- .element: style="float: right; width: 80%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Referee's skills

Is in the game<br>(he/she communicates with players, <br>ONLY reacts to what he/she SAW!) <br><br>Can read the game <br>(he/she is well located on the field, <br>analyses players' behaviour) <!-- .element: style="float: right; width: 80%;" -->

![Referee](images/arbitre.png) <!-- .element: style="width: 20%;" class="plain" -->



## Positionnement de l'arbitre (1/4) <span class="menu-title" style="display: none">Positionnement de l'arbitre (1/4)</span>

Remise en jeu
![Positionnement de l'arbitre](images/position0en.png)



## Referee's location (1/4)

Face-off
![Positionnement de l'arbitre](images/position0en.png)



## Positionnement de l'arbitre (2/4) <span class="menu-title" style="display: none">Positionnement de l'arbitre (2/4)</span>

En cours de jeu
![Positionnement de l'arbitre](images/position1.png) <!-- .element: width="82%" -->



## Referee's location (2/4)

During game
![Referee's location](images/position1.png) <!-- .element: width="82%" -->



## Positionnement de l'arbitre (3/4) <span class="menu-title" style="display: none">Positionnement de l'arbitre (3/4)</span>

Coup franc
![Positionnement de l'arbitre](images/position2.png)



## Referee's location (3/4)

Free-hit
![Referee's location](images/position2.png)



## Positionnement de l'arbitre (4/4) <span class="menu-title" style="display: none">Positionnement de l'arbitre (4/4)</span>

Penalty
![Positionnement de l'arbitre](images/position3.png)



## Referee's location (4/4)

Penalty shot
![Referee's location](images/position3.png)



## Les règles du floorball <span class="menu-title" style="display: none">Les règles du floorball</span>

<br><br>
* Coups francs
* Penalties
* Pénalités de banc (2 min, 5 min, 10 min, match)



## Floorball's rules

<br><br>
* Free-hits
* Penalty shots
* Bench penalties (2 min, 5 min, 10 min, match)



## Coups francs (1/3) <span class="menu-title" style="display: none">Coups francs (1/3)</span>

**Fautes de crosse** <!-- .element: style="float:left;color:orange;" -->
<br>
- **coup** / **attaque** de crosse (involontaire)
- **blocage** / **soulevage** de crosse (en jeu)
- jeu **entre les jambes** (avec la crosse ou la jambe)
- **jeu haut** (crosse située entre le genou et le bassin)

**Fautes de corps** <!-- .element: style="float:left;color:orange;" -->
<br>
- **bousculade** / **obstruction** (charge légère)
- **retenue** de l'adversaire (par ex. tirage de maillot)
- **gêne** (passive) de la relance du gardien
- **reculade** (utiliser le dos comme bouclier)



## Free-hits (1/3)

**Stick offences** <!-- .element: style="float:left;color:orange;" -->
<br>
- **hitting** the opponent's stick (accidentally)
- **blocking** the opponent's stick (while playing the ball)
- playing **between the opponent's legs** (stick or leg)
- playing **above knee level** (between knee and waist)

**Body offences** <!-- .element: style="float:left;color:orange;" -->
<br>
- **pushing** / **obstruction** (soft)
- **holding** the opponent (for instance the jersey)
- **hindering** (passively) the goalkeeper's throw-out
- moving **backward** (using the back as a shield)



## Coups francs (2/3) <span class="menu-title" style="display: none">Coups francs (2/3)</span>

**Fautes de placement** <!-- .element: style="float:left;color:orange;" -->
<br>
- jouer dans la **zone du gardien** (_maison_)
- jouer depuis **l'extérieur du rink**
- être à **moins de 3m** en cas de coup franc adverse

**Autres fautes** <!-- .element: style="float:left;color:orange;" -->
<br>
- **double contact** de la balle avec les pieds
- **passe au gardien**
- **relance du gardien directement hors demi-terrain**
- **relance tardive** (+ 3 sec) du gardien (par ex. balle bloquée)
- **remise en jeu incorrecte** (non frappée)



## Free-hits (2/3)

**Location offences** <!-- .element: style="float:left;color:orange;" -->
<br>
- playing in the opponent **goalkeeper's area**
- playing from **outside of the rink**
- being at a **distance < 3m** during opponent's free-hit

**Other offences** <!-- .element: style="float:left;color:orange;" -->
<br>
- **repeated contact** of ball and lower leg
- **pass to goalkeeper**
- **goalkeeper's throw-out outside of his/her half-field**
- **late throw-out** (+ 3 sec) from goalkeeper (hold if the ball is blocked)
- **uncorrect hit-in**



## Coups francs (3/3) <span class="menu-title" style="display: none">Coups francs (3/3)</span>

**Autres fautes (suite)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **remise en jeu incorrecte** (non frappée, <br>touchée deux fois de suite)
- **déplacement volontaire de la cage adverse**
- **retard de jeu** volontaire
- jeu en **sautant**
- jeu de la tête (NOUVEAU) <!-- .element: style="color:red;" -->



## Free-hits (3/3)

**Other offences (continued)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **uncorrect hit-in** (not hit, touched twice in a row)
- **intentional moving of the opponent's cage**
- **delaying play** intentionally
- playing while **jumping**
- playing with his/her head (NEW) <!-- .element: style="color:red;" -->



## Vidéo (coups francs) <span class="menu-title" style="display: none">Vidéo (coups francs)</span>

<video>
    <source data-src="videos/common-offences.mp4" type="video/mp4" />
</video>



## Video (free-hits)

<video>
    <source data-src="videos/common-offences.mp4" type="video/mp4" />
</video>



## Penalties (1/2) <span class="menu-title" style="display: none">Penalties (1/2)</span>

**Quand ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- faute (de type coup franc ou pénalité) lors d'une action de but
- OU **défense dans la zone** du gardien (_maison_)

**Comment ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- balle jouée avec la crosse depuis le point central
- gardien sur sa ligne au départ du penalty
- balle **non rejouable** si elle est repoussée
- balle en mouvement continu, vers l'avant ou non <!-- .element: style="color:red;" -->



## Penalty shots (1/2)

**When ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- offence (of type free-hit or penalty bench) **in a scoring action**
- OR **defense** in the opponent's **goalkeeper area**

**How ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- ball played with the stick from the centre spot
- goalkeeper starting on his/her line
- ball cannot be played once touched by goal/keeper
- ball follows a continuous, forward or not, movement  <!-- .element: style="color:red;" -->



## Penalties (2/2) <span class="menu-title" style="display: none">Penalties (2/2)</span>

**Et après ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- remise en jeu au point le plus proche si penalty râté,
- remise en jeu au point central sinon

**Autres joueurs** <!-- .element: style="float:left;color:orange;" -->
<br>
- sur le banc de touche
- OU en prison (en cas de pénalité de 5 min ou plus)



## Penalty shots (2/2)

**And then ?** <!-- .element: style="float:left;color:orange;" -->
<br>
- face-off at the nearest spot if penalty shot is missed,
- face-off at the centre spot otherwise

**Other players** <!-- .element: style="float:left;color:orange;" -->
<br>
- in the substitution zone
- OR on the penalty bench (in case of a 5+ penalty)



## Pénalités - prisons (1/6) <span class="menu-title" style="display: none">Pénalités - prisons (1/6)</span>

**2 min (pénalité de banc)** <!-- .element: style="float:left;color:orange;" -->
<br>
- faute **aggravée** (par ex. hors du jeu) ou **répétée**
- **jeu haut** (crosse ou pied au dessus de la hanche)
- **jeu au sol**
- **jeu du bras ou de la main**
- **jeu sans crosse**
- **obstruction (active)** de la relance du gardien
- **positionnement volontaire** à moins de 3m lors d'une remise en jeu
- **obtruction volontaire** (écran) **hors du jeu**



## Penalties (1/6)

**2 min (bench penalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **hard** (e.g. opponent w/o ball) or repeated offence
- **playing** with the stick (or lower leg) **above waist**
- **playing while seating or lying down**
- **playing with the lower arm or hand**
- **playing without stick**
- **hindering** (actively) the goalkeeper's throw-out
- playing while being at a **distance < 3m** during opponent's hit-in / free-hit
- **intentional obstruction** against player w/o ball



## Pénalités - prisons (2/6) <span class="menu-title" style="display: none">Pénalités - prisons (2/6)</span>

**2 min (suite)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **changement hors-zone ou surnombre**
- **contestation** / **antijeu**

**&Agrave; noter** <!-- .element: style="float:left;color:orange;" -->
<br>

En cas de but de l'équipe adverse ET si celle-ci est en surnombre, la pénalité prend fin

Jouer de la tête n'est plus sanctionné par une prison !<!-- .element: style="color:red;" -->

Un penalty n'implique plus forcément de prison !<!-- .element: style="color:red;" -->



## Penalties (2/6)

**2 min (continued)** <!-- .element: style="float:left;color:orange;" -->
<br>
- incorrect substitution / too many players on the rink
- **protest** / **game delay**

**NB** <!-- .element: style="float:left;color:orange;" -->
<br>
In case of goal by the opponent team AND if this team has more players on the field, the penalty terminates

Playing with head no longer implies penalty bench! <!-- .element: style="color:red;" -->

Penalty shots no longer imply bench penalties !<!-- .element: style="color:red;" -->



## Pénalités - prisons (3/6) <span class="menu-title" style="display: none">Pénalités - prisons (3/6)</span>

**5 min (pénalité de banc)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **jeu dangereux** avec la crosse
- **harponnage**
- **jet de crosse** vers la balle
- **charge violente**
- **pousser l'adversaire vers la table de marque / le but**

<br>**&Agrave; noter** <!-- .element: style="float:left;color:orange;" -->
<br>
Une pénalité de 5+ min ne peut pas être interrompue



## Penalties (3/6)

**5 min (bench penalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **dangerous play** with the stick
- **hooking**
- **throwing stick** to hit the ball
- **attacking** opponent
- **throwing opponent against the board / cage**

<br>**NB** <!-- .element: style="float:left;color:orange;" -->
<br>
A 5+ min bench penalty shall not terminate before end



## Pénalités - prisons (4/6) <span class="menu-title" style="display: none">Pénalités - prisons (4/6)</span>

**10 min (pénalité personnelle)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **comportement anti-sportif**

**&Agrave; noter** <!-- .element: style="float:left;color:orange;" -->
<br>
- une pénalité personnelle (10 min) s'accompagne **toujours** d'une pénalité de banc (2 ou 5 min)
- un joueur (choix capitaine) accompagne le fautif
- le fautif ne rentre en jeu qu'après 12 min (**incompressible**) ET lors d'un arrêt de jeu
- l'accompagnateur rentre en jeu après 2 min



## Penalties (4/6)

**10 min (personal penalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **unsportsmanlike behaviour**

**NB** <!-- .element: style="float:left;color:orange;" -->
<br>
- a personal penalty **always** comes with a bench penalty (2 or 5 min)
- a player (captain's choice) accompanies the offender
- the offender only leaves bench after 12 min (**unstoppable**) AND during an interruption
- the other player leaves bench after 2 min



## Pénalités - prisons (5/6) <span class="menu-title" style="display: none">Pénalités - prisons (5/6)</span>

**1-match (pénalité de match)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **crosse non conforme**
- **joueur non qualifié** (pas sur feuille de match)
- **comportement antisportif répété**
- **agression**

**&Agrave; noter** <!-- .element: style="float:left;color:orange;" -->
<br>
- une pénalité de match s'accompagne toujours d'une pénalité de banc de 5 min
- le joueur fautif doit regagner le vestiaire



## Penalties (5/6)

**1-match (match pernalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **illegal stick**
- **illegal player** (not on the match record)
- **repeated unsportsmanlike behaviour**
- **agression**

**NB** <!-- .element: style="float:left;color:orange;" -->
<br>
- a match penalty always comes with a 5 min bench penalty
- offender shall go back to the dressing room



## Pénalités - prisons (6/6) <span class="menu-title" style="display: none">Pénalités - prisons (6/6)</span>

**2-match (pénalité de match)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **altercation**
- **tricherie** / **sabotage**

**3-match (pénalité de match)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **bagarre**
- **comportement violent**

**&Agrave; noter** <!-- .element: style="float:left;color:orange;" -->
<br>
- "N-match" indique une suspension de N matchs



## Penalties (6/6)

**2-match (match pernalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **scuffle**
- **cheating** / **sabotage**

**3-match (match pernalty)** <!-- .element: style="float:left;color:orange;" -->
<br>
- **fight**
- **violent behaviour**

**NB** <!-- .element: style="float:left;color:orange;" -->
<br>
- "N-match" means exclusion for N matches



## Vidéo (pénalités de banc) <span class="menu-title" style="display: none">Vidéo (pénalités de banc)</span>

<video>
    <source data-src="videos/bench-penalties.mp4" type="video/mp4" />
</video>



## Video (bench penalties)

<video>
    <source data-src="videos/bench-penalties.mp4" type="video/mp4" />
</video>



## Détails qui comptent! (1/4) <span class="menu-title" style="display: none">Détails qui comptent! (1/4)</span>

Remise en jeu (bully) <!-- .element: style="float:left;color:orange;" -->
<br>
- pieds **perpendiculaires** à la ligne centrale et à la **même distance**
- position des mains **normale** ET **au dessus de la marque** de crosse
- **palette perpendiculaire à la ligne centrale**
- la palette est posée d'abord par le joueur de l'**équipe visiteuse** si bully au point central, et par celui de l'**équipe défendante** sinon
- autres **joueurs à plus de 3m**, de leur côté



## Hell is in the details! (1/4)

Face-off <!-- .element: style="float:left;color:orange;" -->
<br>
- feet **perpendicular to the centre line** and at same distance
- stick hold with **normal grip** and with both hands **above grip mark**
- blades **perpendicular to the centre line**
- blade side is first chosen by **visiting team** if in centre spot, by **defending team** otherwise
- all other players shall be **at 3+ m**, and on their side



## Détails qui comptent! (2/4) <span class="menu-title" style="display: none">Détails qui comptent! (2/4)</span>

**Règle de l'avantage** <!-- .element: style="float:left;color:orange;" -->
<br>
- quand l'équipe non-fautive **conserve la balle** ET/OU<br> est dans
une **meilleure** position qu'un coup franc
- en fin d'avantage (balle perdue ou situation défavorable), on
**revient à l'endroit de la faute**
- l'avantage ne dure que **quelques secondes**
- l'avantage est appliqué au **choix de l'arbitre** (à lui de juger du
caractère favorable de la situation de jeu)
- l'avantage est **indiqué clairement** (parole + geste)
- peu mener à une **pénalité OU un penalty différé**



## Hell is in the details! (2/4)

**Advantage** <!-- .element: style="float:left;color:orange;" -->
<br>
- when the non-offending team **still controls** the ball OR/AND is in a **better position** than a free-hit
- when the non-offending team loses the ball, game is resumed **where the offence took place**
- an advantage can last **a few secondes**
- **referee decides** whether an advantage should be granted (he/she decides what a better situation is)
- an advantage is announced **clearly** (voice+signal)
- can lead to a **bench penalty OR penalty shot**



## Détails qui comptent! (3/4) <span class="menu-title" style="display: none">Détails qui comptent! (3/4)</span>

**Pénalité différée** <!-- .element: style="float:left;color:orange;" -->
<br>
- est indiquée par un **bras levé de l'arbitre**
- **sortie du gardien** de l'équipe non-fautive **possible** (car sans risque)
- peut s'accompagner d'un **penalty différé**
- est appliquée **dès que** l'équipe non-fautive essaye de gagner du temps (arrêt du jeu par l'arbitre)
- **remise en jeu** (bully) lorsque l'équipe fautive a récupéré la balle (après application de la pénalité)



## Hell is in the details! (3/4)

**Delayed penalty** <!-- .element: style="float:left;color:orange;" -->
<br>
- is indicated by an **arm held vertically**
- **goalkeeper's subsitution** from non-offending team is **possible** (cause not risky)
- can come with a **delayed penalty shot**
- is realised **as soon as** the non-offending team tries to delay game (interruption by referee)
- **face-off** when the non-offending team gains the ball (once the bench penalty is recorded)



## Détails qui comptent! (4/4) <span class="menu-title" style="display: none">Détails qui comptent! (4/4)</span>

**But accordé** <!-- .element: style="float:left;color:orange;" -->
<br>
- balle jouée **avec la crosse**, passant **complètement la ligne** de but et frappe **non précédée d'une faute** de l'équipe attaquante
- OU **Contre-Son-Camp** (OG sur la feuille de match)

**But refusé** <!-- .element: style="float:left;color:orange;" -->
<br>
- **balle déviée** par le corps d'un joueur attaquant,
- OU balle passe la ligne **après le coup de sifflet**,
- OU but **marqué directement par relance du gardien**
- OU équipe sous le coup d'une **pénalité différée**



## Hell is in the details! (4/4)

**Allowed goal** <!-- .element: style="float:left;color:orange;" -->
<br>
- ball played **with the stick**, **entirely passes** the goal line and shot **not preceded with an offence** from the attacking team
- OR **Own-Goal** (OG on the match record)

**Refused goal** <!-- .element: style="float:left;color:orange;" -->
<br>
- ball **kicked by the body** of an attacking player,
- OR ball passing the goal line **after signal**,
- OR goal scored **directly at goalkeeper's throw-out**
- OR team **under a delayed penalty**



## Vidéo (gestes de l'arbitre) <span class="menu-title" style="display: none">Vidéo (gestes de l'arbitre)</span>

<video>
    <source data-src="videos/signals.mp4" type="video/mp4" />
</video>



## Video (referee signals)

<video>
    <source data-src="videos/signals.mp4" type="video/mp4" />
</video>



## &Agrave; retenir (1/2) <span class="menu-title" style="display: none">&Agrave; retenir (1/2)</span>

**Principales modifications des règles au 01/07/2018** <!-- .element: style="float:left;color:orange;" -->
<br>
- jouer de la **tête ne provoque plus de prison**

- penalty et pénalité de banc sont **traités indépendemment**

- lors d'un penalty, la **balle peut reculer** (tant que le mouvement est continu)



## To keep in mind (1/2)

**Main modifications of the rules as of July, 1st 2018** <!-- .element: style="float:left;color:orange;" -->
<br>
- playing with the **head no longer leads to bench penalty**

- penalty shot and bench penalties **are processed independently from each other**

- during penalty shot, the **ball can move backwards** (as long as the movement is continuous)



## &Agrave; retenir (2/2) <span class="menu-title" style="display: none">&Agrave; retenir (2/2)</span>

- connaître les règles est **important pour ne pas pénaliser son équipe** (ne pas commettre de faute _fatale_ par ignorance)
- garder à l'esprit que l'**arbitre est aussi un joueur** et peut commettre des fautes _involontaires_
- faire **respecter les règles assainit le match** (réduction des frustrations et actes violents) et **augmente la qualité de jeu**



## To keep in mind (2/2)

- knowing rules is **important to avoid penalising his/her own team** (to avoid _fatal_ offences due to ignorance)
- keep in mind that **referees are also players** and can make offences _unwillingly_
- get players to play by the rules **impacts the game's atmosphere** (less frustation and violent behaviours) and **raises game quality**



## Références <span class="menu-title" style="display: none">Références</span>

<br>
- [Règles officielles du Floorball (IFF, Juillet 2018)](http://www.floorball.org/pages/EN/Rules-of-the-game-)
- [Documents de la Fédération Française de FLoorball (FFFL)](http://www.floorball.fr/Arbitrage) :
  - ["L'essentiel de l'arbitrage"](http://www.floorball.fr/IMG/pdf/IFF_-_L_Essentiel_de_l_Arbitrage_-_V_Grieu_2012-3.pdf) par V. Grieu
  - ["Support Formation Arbitrage"](http://www.floorball.fr/IMG/pdf/Support_formation_-_Secretariat_de_match.pdf) par V. Grieu
- Vidéos : [http://floorballcoach.org](http://floorballcoach.org)



## References

<br>
- [Official Floorball Rules (IFF, July 2018)](http://www.floorball.org/pages/EN/Rules-of-the-game-)
- [Documents from the Fédération Française de FLoorball (FFFL)](http://www.floorball.fr/Arbitrage):
  - ["L'essentiel de l'arbitrage"](http://www.floorball.fr/IMG/pdf/IFF_-_L_Essentiel_de_l_Arbitrage_-_V_Grieu_2012-3.pdf) by V. Grieu
  - ["Support Formation Arbitrage"](http://www.floorball.fr/IMG/pdf/Support_formation_-_Secretariat_de_match.pdf) by V. Grieu
- Videos from [http://floorballcoach.org](http://floorballcoach.org)



## Credits <span class="menu-title" style="display: none">Credits</span>

<br>
- Realised by [Yannick Parmentier](mailto:parmentier.floorball@gmail.com) ([Loups Lorrains ASPTT Laxou Floorball](https://nancy-meurthe-et-moselle-floorball.asptt.com/))
<br><br>
- Special thanks to [Julien Mesona](mailto:mesona.floorball@gmail.com) ([ECO Fleury Floorball](http://lescoyotesdefleury.fr)) for his help
<br><br>
- Made with [Reveal.js](https://revealjs.com)
